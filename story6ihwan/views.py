from django.shortcuts import render

# Create your views here.
from .models import Landing
from .forms import LandingForm
from django.http import HttpResponseRedirect


def landing(request):
    form = LandingForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/landing')
    theData = Landing.objects.all()
    context = {
        'form': form,
        'data': theData
    }
    return render(request, 'landing.html', context)


def profile(request):
    return render(request, 'profile.html')
# Create your views here.
