from django.db import models

# Create your models here.
from django.db import models
from django.utils import timezone


class Landing(models.Model):
    status = models.TextField(max_length=300)
    time = models.DateTimeField(auto_now=True)
