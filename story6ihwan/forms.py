from django import forms
from .models import Landing


class LandingForm(forms.ModelForm):
    class Meta:
        model = Landing
        fields = "__all__"
        widgets = {
            'status': forms.Textarea(attrs={'placeholder': 'Your Status', 'id': 'stats'})

        }
        labels = {
            'status': '',
        }
